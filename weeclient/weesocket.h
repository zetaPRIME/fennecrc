#pragma once

#include <memory>
#include <functional>

#include <QObject>
#include <QTcpSocket>

#include "data/weemsg.h"

class WeeSocket : public QObject {
    Q_OBJECT

    std::unique_ptr<QTcpSocket> tcp;
    QByteArray msgBuf;
    int msgBytesExpected;

    QString hostname, password;
    quint16 port;
    bool isCompressed = false;

    uint64_t nextId = 0;

    void onConnected();
    void onDisconnected();
    void onReadyRead();

public:
    explicit WeeSocket(QObject *parent = nullptr);

    void connectToHost(const QString& hostname, quint16 port, const QString& password, bool ssl = true, bool compression = true);
    void sendCommand(const QString& cmd);
    void sendCommand(const QString& cmd, std::function<void (const WeeMsg&)> callback);

signals:
    void recievedMessage(const WeeMsg&);

};
