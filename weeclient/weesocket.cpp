#include "weesocket.h"

#include "data/util.h"

#include <QSslSocket>

#include <QDebug>

WeeSocket::WeeSocket(QObject *parent) : QObject(parent) {

}

void WeeSocket::connectToHost(const QString &hostname, quint16 port, const QString &password, bool ssl, bool compression) {
    this->hostname = hostname;
    this->password = password;
    this->port = port;
    this->isCompressed = compression;

    // reset buffer
    msgBuf.clear();
    msgBytesExpected = 0;

    // create socket...
    if (ssl) tcp.reset(new QSslSocket(this));
    else tcp.reset(new QTcpSocket(this));

    // and hook up
    connect(&*tcp, &QAbstractSocket::connected, this, &WeeSocket::onConnected);
    connect(&*tcp, &QAbstractSocket::disconnected, this, &WeeSocket::onDisconnected);
    connect(&*tcp, &QAbstractSocket::readyRead, this, &WeeSocket::onReadyRead);

    tcp->connectToHost(hostname, port);
}

void WeeSocket::sendCommand(const QString& cmd) {
    tcp->write(cmd.toUtf8());
    tcp->write("\n", 1); // terminate
}

void WeeSocket::sendCommand(const QString& cmd, std::function<void (const WeeMsg&)> callback) {
    QString id = QString::number(nextId++, 16);
    sendCommand(QS("(%1) %2").arg(id).arg(cmd));

    // callback
    auto conn = std::make_shared<QMetaObject::Connection>();
    *conn = QObject::connect(this, &WeeSocket::recievedMessage, [conn, id, c = std::move(callback)](const WeeMsg& msg) {
        if (msg.id() == id) {
            QObject::disconnect(*conn); // unhook
            c(msg); // and execute
        }
    });
}


void WeeSocket::onConnected() {
    sendCommand(QS("init password=%1,compression=%2").arg(password.replace(",","\\,")).arg(isCompressed ? QS("on") : QS("off")));
    sendCommand(QS("sync"));
}

void WeeSocket::onDisconnected() {
    qDebug() << "socket disconnected";
}

void WeeSocket::onReadyRead() {
    if (msgBytesExpected == 0) msgBytesExpected = 4; // length
    while (tcp->bytesAvailable() > 0) {
        if (msgBuf.size() < msgBytesExpected) {
            msgBuf.append(tcp->read(msgBytesExpected - msgBuf.size()));
            if (msgBuf.size() >= msgBytesExpected) {
                if (msgBytesExpected == 4) { // extract length and continue
                    msgBytesExpected = getSize32(msgBuf.data());
                } else { // finish message
                    //bool msgCompressed = static_cast<char>(msgBuf[4]) != 0;
                    auto m = WeeMsg::fromRaw(msgBuf);
                    msgBuf.clear(); // reset buffer before emit
                    msgBytesExpected = 0;
                    emit recievedMessage(m);
                }
            }
        } else { // assume error
            qDebug() << "msg overread, size expected" << msgBytesExpected << "actual size" << msgBuf.size();
            msgBuf.clear();
            msgBytesExpected = 0;
            onReadyRead();
        }
    }
}
