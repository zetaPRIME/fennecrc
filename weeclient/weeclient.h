#pragma once

#include <memory>

#include <QObject>

class WeeSocket;
class WeeClient : public QObject {
    Q_OBJECT

public:
    std::shared_ptr<WeeSocket> socket;

    WeeClient(QObject* = nullptr);

    void connectToHost(const QString& hostname, quint16 port, const QString& password, bool ssl = true, bool compression = true);

signals:


};
