#include "weeclient.h"
#include "weesocket.h"

WeeClient::WeeClient(QObject* parent) : QObject(parent) {
    socket = std::make_shared<WeeSocket>(this);
}

void WeeClient::connectToHost(const QString &hostname, quint16 port, const QString &password, bool ssl, bool compression) {
    socket->connectToHost(hostname, port, password, ssl, compression);
}
