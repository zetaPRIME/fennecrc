QT -= gui
QT += network

TEMPLATE = lib
CONFIG += staticlib c++17

DEFINES += QT_DEPRECATED_WARNINGS
DEFINES += MINIZ_NO_STDIO MINIZ_NO_TIME MINIZ_NO_ARCHIVE_APIS
DEFINES += QS=QStringLiteral

SOURCES += $$files(*.cpp, true) $$files(*.c, true)
HEADERS += $$files(*.h, true) $$files(*.hpp, true)
FORMS += $$files(*.ui, true)

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target
