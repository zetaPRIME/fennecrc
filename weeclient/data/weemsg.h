#pragma once

#include "weeobj.h"

#include <memory>

#include <QByteArray>
#include <QString>

struct WeeMsg_p;
class WeeMsg {
    std::shared_ptr<WeeMsg_p> d;

public:
    WeeMsg();
    WeeMsg(const WeeMsg&);
    WeeMsg(const WeeMsg&&);

    QString id() const;
    int count() const;
    WeeObj operator[](int) const;

    static WeeMsg fromRaw(QByteArray);
};
