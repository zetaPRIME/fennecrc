#include "weemsg.h"

#include "util.h"
#include "weeobj.h"

#include <QVector>

struct WeeMsg_p {
    QString id;
    QVector<WeeObj> data;
};

WeeMsg::WeeMsg() { d = std::make_shared<WeeMsg_p>(); }
WeeMsg::WeeMsg(const WeeMsg& o) { d = o.d; }
WeeMsg::WeeMsg(const WeeMsg&& o) { d = o.d; }

QString WeeMsg::id() const { return d->id; }
int WeeMsg::count() const { return d->data.count(); }
WeeObj WeeMsg::operator[](int i) const { if (i >= d->data.count()) return {nullptr}; return d->data[i]; }

WeeMsg WeeMsg::fromRaw(QByteArray buf) {
    WeeMsg msg;
    auto raw = buf.data();
    bool msgCompressed = raw[4] != 0; // TODO: actually implement compression

    // for now, just assume
    char* data = raw + 5;
    char* end = raw + buf.size();

    msg.d->id = readString(data);

    // and parse out fully
    while (data < end) msg.d->data.push_back(WeeObj::parse(data));

    return msg;
}
