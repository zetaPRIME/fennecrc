#pragma once

#include <QString>
#include <QByteArray>

static inline uint getSize32(const char* data) {
    return static_cast<uint>(data[0]) << 24 |
           static_cast<uint>(data[1]) << 16 |
           static_cast<uint>(data[2]) << 8 |
           static_cast<uint>(data[3]);
}
static inline uint readSize32(char*& data) {
    uint s = getSize32(data);
    data += 4;
    return s;
}
//static inline QString getString(const char* data) { return QString::fromUtf8(data+4, size32(data)); }
static inline QString readString(char*& data) {
    uint s = readSize32(data);
    if (s == UINT32_MAX) s = 0; // null string
    auto qs = QString::fromUtf8(data, s);
    data += s;
    return qs;
}
static inline QByteArray readData(char*& data) {
    uint s = readSize32(data);
    if (s == UINT32_MAX) s = 0; // null buffer
    auto d = QByteArray(data, s);
    data += s;
    return d;
}
static inline QString readShortString(char*& data) {
    uchar s = *(data++);
    auto qs = QString::fromUtf8(data, s);
    data += s;
    return qs;
}
static inline QByteArray readShortData(char*& data) {
    uchar s = *(data++);
    auto qs = QByteArray(data, s);
    data += s;
    return qs;
}
