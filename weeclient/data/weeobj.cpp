#include "weeobj.h"

#include "util.h"

#include <array>
#include <algorithm>

#include <QVector>
#include <QHash>

#include <QDebug>

struct WeeObj_p {
    WeeObj_p() { }
    virtual ~WeeObj_p() { }
    WeeObj::ObjType type = WeeObj::NUL;
    QString name;
};

struct WeeObjChar_p : public WeeObj_p {
    ~WeeObjChar_p() override { }
    char value;
    WeeObjChar_p(const char& v) : value(v) { type = WeeObj::CHR; }
};
struct WeeObjInt_p : public WeeObj_p {
    ~WeeObjInt_p() override { }
    int value;
    WeeObjInt_p(const int& v) : value(v) { type = WeeObj::INT; }
};
struct WeeObjString_p : public WeeObj_p {
    ~WeeObjString_p() override { }
    QByteArray data;
    WeeObjString_p(const QByteArray& v) : data(v) { type = WeeObj::STR; }
};
struct WeeObjList_p : public WeeObj_p {
    ~WeeObjList_p() override { }
    QVector<WeeObj> data;
    WeeObjList_p() { type = WeeObj::LST; }
};
struct WeeObjMap_p : public WeeObj_p {
    ~WeeObjMap_p() override { }
    QHash<QString, WeeObj> data;
    WeeObjMap_p() { type = WeeObj::MAP; }
};

WeeObj::WeeObj(std::shared_ptr<WeeObj_p> dd) : d(dd) { }
WeeObjChar::WeeObjChar(std::shared_ptr<WeeObj_p> dd) : WeeObj(dd) { }
WeeObjInt::WeeObjInt(std::shared_ptr<WeeObj_p> dd) : WeeObj(dd) { }
WeeObjString::WeeObjString(std::shared_ptr<WeeObj_p> dd) : WeeObj(dd) { }
WeeObjList::WeeObjList(std::shared_ptr<WeeObj_p> dd) : WeeObj(dd) { }
WeeObjMap::WeeObjMap(std::shared_ptr<WeeObj_p> dd) : WeeObj(dd) { }

WeeObjChar WeeObj::toChar() {
    if (d && d->type == CHR) return {d};
    return {nullptr};
}
WeeObjInt WeeObj::toInt() {
    if (d && d->type == INT) return {d};
    return {nullptr};
}
WeeObjString WeeObj::toString() {
    if (d && d->type == STR) return {d};
    return {nullptr};
}
WeeObjList WeeObj::toList() {
    if (d && d->type == LST) return {d};
    return {nullptr};
}
WeeObjMap WeeObj::toMap() {
    if (d && d->type == MAP) return {d};
    return {nullptr};
}

QString WeeObj::name() const { if (d) return d->name; return QS(); }

char WeeObjChar::value() const { if (!d) return 0; return std::static_pointer_cast<WeeObjChar_p>(d)->value; }
int WeeObjInt::value() const { if (!d) return 0; return std::static_pointer_cast<WeeObjInt_p>(d)->value; }
QString WeeObjString::value() const { if (!d) return QS(); return QString::fromUtf8(std::static_pointer_cast<WeeObjString_p>(d)->data); }
const QByteArray WeeObjString::raw() const { if (!d) return {}; return std::static_pointer_cast<WeeObjString_p>(d)->data; }
WeeObj WeeObjList::operator[](int idx) const {
    if (!d) return {nullptr};
    auto dd = std::static_pointer_cast<WeeObjList_p>(d);
    if (idx >= dd->data.count()) return {nullptr};
    return dd->data[idx];
}
WeeObj WeeObjMap::operator[](const QString& key) const {
    if (!d) return {nullptr};
    auto dd = std::static_pointer_cast<WeeObjMap_p>(d);
    if (!dd->data.contains(key)) return {nullptr};
    return dd->data.value(key);
}

WeeObj WeeObj::parse(char*& data) {
    data += 3;
    return parse(data, data-3);
}

WeeObj WeeObj::parse(char*& data, const char* type) {
    auto t = QString::fromUtf8(type, 3);
    /**/ if (t == QS("chr")) return {std::make_shared<WeeObjChar_p>(*(data++))}; // get char, then increment
    else if (t == QS("int")) {
        return {std::make_shared<WeeObjInt_p>(readSize32(data))};
    } else if (t == QS("lon") || t == QS("ptr") || t == QS("tim")) { // 8bit-size string
        return {std::make_shared<WeeObjString_p>(readShortData(data))};
    } else if (t == QS("str") || t == QS("buf")) { // 32bit-size string
        return {std::make_shared<WeeObjString_p>(readData(data))};
    } else if (t == QS("arr")) { // array, type of list
        char* etype = data;
        uint count = getSize32(data+3);
        data += 7;
        auto d = std::make_shared<WeeObjList_p>();
        d->data.reserve(count);
        for (uint i = 0; i < count; i++) d->data.push_back(parse(data, etype));
        return {d}; // elements take care of advancing
    } else if (t == QS("inf")) { // info, pair of strings
        auto d = std::make_shared<WeeObjList_p>();
        d->data.reserve(2);
        d->data.push_back(parse(data, "str"));
        d->data.push_back(parse(data, "str"));
        return {d}; // elements take care of advancing
    } else if (t == QS("htb")) { // hashtable, plain ol' map
        char* ktype = data, *vtype = data+3;
        data += 6;
        uint count = readSize32(data);
        auto d = std::make_shared<WeeObjMap_p>();
        for (uint i = 0; i < count; i++) {
            auto ko = parse(data, ktype);
            auto vo = parse(data, vtype);
            if (auto k = ko.toString()) d->data.insert(k, vo);
            else if (auto k = ko.toInt()) d->data.insert(QString::number(k.value()), vo);
            else qDebug() << "unsupported hashtable key of type" << QString::fromUtf8(ktype, 3);
        }
        return {d};
    } else if (t == QS("inl")) { // infolist, named list of maps
        auto d = std::make_shared<WeeObjList_p>();
        d->name = readString(data);
        uint lcount = readSize32(data);
        for (uint i = 0; i < lcount; i++) {
            auto dd = std::make_shared<WeeObjMap_p>();
            uint vcount = readSize32(data);
            for (uint j = 0; j < vcount; j++) {
                auto k = readString(data);
                auto v = parse(data);
                dd->data.insert(k, v);
            }
            d->data.push_back({dd});
        }
        return {d};
    } else if (t == QS("hda")) { // hdata, list of structs
        auto path = readString(data);
        auto ptrCount = path.count('/')+1;
        auto s_keys = readString(data);
        uint itemCount = readSize32(data);
        QVector<std::pair<QString, QByteArray>> proto;
        for (auto k : s_keys.split(',')) { // assemble prototype
            auto ks = k.split(':');
            proto.push_back({ks[0], ks[1].toUtf8()});
        }
        auto list = std::make_shared<WeeObjList_p>();
        for (uint i = 0; i < itemCount; i++) {
            auto obj = std::make_shared<WeeObjMap_p>();
            {
                auto ppath = std::make_shared<WeeObjList_p>();
                for (int p = 0; p < ptrCount; p++) ppath->data.push_back({std::make_shared<WeeObjString_p>(readShortData(data))});
                obj->data.insert(QS("_path"), {ppath});
            }
            for (auto& pt : proto) obj->data.insert(pt.first, parse(data, pt.second.data()));
            list->data.push_back({obj});
        }
        return {list};
    }

    // anything else is an unknown type
    return {nullptr};
}

