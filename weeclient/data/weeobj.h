#pragma once

#include <memory>

#include <QString>

class WeeObjChar;
class WeeObjInt;
class WeeObjString;
class WeeObjMap;
class WeeObjList;
// hdata: list of maps
// info: list of two strings

struct WeeObj_p;
class WeeObj {
protected:
    std::shared_ptr<WeeObj_p> d;
public:
    enum ObjType : char { NUL, CHR, INT, STR, MAP, LST };

    WeeObj(std::shared_ptr<WeeObj_p>);
    WeeObj() : WeeObj(nullptr) { }
    virtual ~WeeObj() { };

    // validity check
    operator bool() const { return d != nullptr; }
    QString name() const;
    static WeeObj parse(char*&);
    static WeeObj parse(char*&, const char* type);

    WeeObjChar toChar();
    WeeObjInt toInt();
    WeeObjString toString();
    WeeObjList toList();
    WeeObjMap toMap();
};

class WeeObjChar : public WeeObj {
public:
    WeeObjChar(std::shared_ptr<WeeObj_p>);
    char value() const;
    operator char() const { return value(); }
};

class WeeObjInt : public WeeObj {
public:
    WeeObjInt(std::shared_ptr<WeeObj_p>);
    int value() const;
    operator int() const { return value(); }
};

class WeeObjString : public WeeObj {
public:
    WeeObjString(std::shared_ptr<WeeObj_p>);
    QString value() const;
    operator QString() const { return value(); }
    const QByteArray raw() const;
    operator const QByteArray() const { return raw(); }
};

class WeeObjList : public WeeObj {
public:
    WeeObjList(std::shared_ptr<WeeObj_p>);
    WeeObj operator[](int) const;
};

class WeeObjMap: public WeeObj {
public:
    WeeObjMap(std::shared_ptr<WeeObj_p>);
    WeeObj operator[](const QString&) const;
};
