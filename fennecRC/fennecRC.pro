QT += core gui network
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += $$files(*.cpp, true)
HEADERS += $$files(*.h, true) \
           $$files(*.hpp, true)
FORMS += $$files(*.ui, true)

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../weeclient/release/ -lweeclient
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../weeclient/debug/ -lweeclient
else:unix: LIBS += -L$$OUT_PWD/../weeclient/ -lweeclient

INCLUDEPATH += $$PWD/../weeclient
DEPENDPATH += $$PWD/../weeclient

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../weeclient/release/libweeclient.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../weeclient/debug/libweeclient.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../weeclient/release/weeclient.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../weeclient/debug/weeclient.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../weeclient/libweeclient.a
