#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "weeclient.h"
#include "weesocket.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);

    auto c = new WeeClient(this);
    c->connectToHost("localhost", 9000, "just testing", false, false);
    QObject::connect(c->socket.get(), &WeeSocket::recievedMessage, this, [](WeeMsg msg) {
        qDebug() << "hey there yo";
    });
}

MainWindow::~MainWindow() {
    delete ui;
}

